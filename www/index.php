<!doctype html>
<html class="no-js" lang="">
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>MMI2 TP1</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" href="css/normalize.css">
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/owl.carousel.css">
	<link rel="stylesheet" href="css/owl.theme.default.css">
	<link rel="stylesheet" href="css/remodal-default-theme.css">
	<link rel="stylesheet" href="css/remodal.css">
	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed|Roboto+Mono" rel="stylesheet">
</head>
<body>
<div class="layout remodal-bg">
	<header role="banner">
		<div class="wrapper" data-relative-input="true" id="scene">
			<p class="logo" data-depth="0.6">
				<a href="#" data-page="accueil">
					Mon portfolio
				</a>
			</p>
		</div>
		<nav role="navigation">
			<div class="wrapper">
				<ul>
					<li><a href="index.php" data-page="accueil"><i class="fa fa-fw fa-home"></i> Accueil</a></li>
					<li><a href="#" data-page="galerie">Galerie</a></li>
					<li><a href="#" data-page="curriculum"><i class="fa fa-fw fa-graduation-cap"></i> Curriculum</a></li>
					<li><a href="#" data-remodal-target="contact"><i class="fa fa-fw fa-envelope"></i> Contact</a></li>
				</ul>
			</div>
		</nav>
	</header>
	<main>
		<div class="wrapper">
			<div id="pages">
				<div class="owl-carousel owl-theme slider">
					<img src="http://lorempicsum.com/rio/800/300/1" alt="slider 1" width="800" height="300" class="img-responsive">
					<img src="http://lorempicsum.com/rio/800/300/2" alt="slider 2" width="800" height="300" class="img-responsive">
					<img src="http://lorempicsum.com/rio/800/300/3" alt="slider 3" width="800" height="300" class="img-responsive">
					<img src="http://lorempicsum.com/rio/800/300/4" alt="slider 4" width="800" height="300" class="img-responsive">
				</div>
				<article>
					<h1>Accueil</h1>
					<p>Lorem ipsum dolor sit amet, <a href="https://github.com/vodkabears/Remodal">remodal</a> adipisicing elit. Aut, autem, consequatur, cupiditate delectus error harum
						modi nam nesciunt nulla officia omnis quas quod quos rem repellendus repudiandae saepe sunt totam voluptate
						voluptatibus! A <a href="http://www.jacklmoore.com/autosize/">autosize</a> ad aliquid asperiores aspernatur assumenda aut consectetur, culpa debitis dolor
						doloremque dolores error esse ex fugit impedit inventore ipsa ipsum iure, iusto obcaecati officia quod repellendus
						sint tenetur unde velit vero! <a href="http://google.com">Asperiores</a> facere illo, laboriosam mollitia saepe tempora. Animi aut delectus
						doloremque facere in incidunt, molestiae, nisi nostrum numquam officiis quae quas quos repudiandae tenetur, ut.
						Aperiam culpa exercitationem expedita iste modi obcaecati odit repellendus ullam? Fuga laudantium nostrum numquam
						possimus quidem quod sapiente sed sit vel, vero. Aliquam beatae commodi distinctio dolor doloribus eius eligendi ex,
						id illo iure labore laboriosam laudantium libero magnam, minima neque, nesciunt non quasi rem sed sunt ut velit
						veniam. Beatae culpa cumque dolor harum magni modi neque porro, sit!</p>
					<h2>Lorem ipsum dolor sit.</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad adipisci architecto consequuntur
						debitis delectus dicta laborum nam, nobis numquam perspiciatis quidem rem repellat repudiandae
						sint veniam veritatis, vero <a href="https://api.jquery.com/">jQuery documentation</a>. Aliquid asperiores aut beatae cupiditate doloremque
						dolores, eveniet illum in maxime molestiae natus officiis optio quasi ratione repellat soluta,
						tempora unde.</p>
					<p>Alias dolore eius est fuga nam quae sunt temporibus. Ad consequatur error facere incidunt minima
						modi nam necessitatibus officiis quam reiciendis! Accusamus commodi ea iure neque numquam
						obcaecati odio porro saepe, voluptas voluptatibus! A ab aliquid aspernatur cum eius, et illo,
						incidunt officiis pariatur quam qui, quibusdam quod. Nam, sapiente!</p>
					<h2>Lorem ipsum dolor sit.</h2>
					<p>Adipisci consequatur culpa deleniti dicta <a href="//owlcarousel2.github.io/OwlCarousel2/">owl-carousel</a> facere ipsa odit. Accusantium animi aut
						cumque dolore fugit ipsum maiores nesciunt qui sunt voluptatem? Aperiam cupiditate delectus
						doloribus eaque earum et harum, id impedit in iusto magnam molestiae nulla odit quae saepe
						sapiente sequi. Debitis eveniet laudantium molestiae provident quos repudiandae rerum sed.</p>
				</article>
			</div>
		</div>
	</main>
	<footer>
		<div class="wrapper">
			<ul>
				<li class="social"><a href="//twitter.com"><i class="fa fa-fw fa-twitter"></i></a></li>
				<li class="social"><a href="//twitter.com"><i class="fa fa-fw fa-facebook"></i></a></li>
			</ul>
		</div>
	</footer>
</div>

<div class="remodal" data-remodal-id="contact">
	<button data-remodal-action="close" class="remodal-close"></button>
	<h1>Contact</h1>
	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur ipsam itaque minima, nesciunt numquam, odio officia quia quo quod recusandae tempore totam voluptatem, voluptatum. Minima molestias nemo nihil perferendis ullam.</p>
	<br>
	<form action="">
		<div class="form-group"><label for="name">Nom</label><input id="name" name="name" type="text" class="form-control"></div>
		<div class="form-group"><label for="mail">Email</label><input id="mail" name="mail" type="text" class="form-control"></div>
		<div class="form-group"><label for="message">Votre message</label><textarea name="message" id="message" cols="20" rows="5" class="form-control"></textarea></div>
	</form>
	<button data-remodal-action="cancel" class="remodal-cancel">Cancel</button>
	<button data-remodal-action="confirm" class="remodal-confirm">Envoyer</button>
</div>

<script src="https://use.fontawesome.com/601a2e39a6.js"></script>
<script src="js/jquery.js"></script>
<script src="js/jquery.owl.carousel.js"></script>
<script src="js/remodal.js"></script>
<script src="js/autosize.js"></script>
<script src="js/app.js"></script>

</body>
</html>
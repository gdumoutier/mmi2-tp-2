	(function($){

	/*=========================================\
		$SLIDER
	\=========================================*/

	function init_carousel() {
		$('.slider').owlCarousel({
			items: 1
		});
	}
	init_carousel();

	/*=========================================\
		$AUTOGROW
	\=========================================*/

	autosize($('textarea'));
	
	/*=========================================\
		$LOADPAGE   
	\=========================================*/

	// Pour vous aider n1
	//console.log($('.logo').attr('class'));

	// Pour vous aider n2
	//$.get('pages/accueil.html',function( data ) {
	//	console.log(data);
	//});

	function loadPage(target){
		alert(target);
	}

	loadPage('Regardez votre code JS');

})(jQuery);
#TP MMI2 : mise en application jQuery

### **TP 1** : 

- Suivre les consignes sans éditer l'index.php à l'exception de l'ajout d'appel **css** ou **js**. Aidez-vous du document pdf fourni.
- Inclure la librairie **jQuery** du CDN de votre choix et y appliquer un *Fallback* (ou solution de rechange) vers un **jQuery** local.
- Faire une mise en page CSS basique : header, nav, footer... dans un fichier **style.css** que vous placerez dans un sous-dossier **/css**
- Créer un fichier **app.js** dans le sous dossier **/js**
- Dans ce fichier, instancier **jQuery** dans les meilleurs conditions
- ... puis instancier **[owlCarousel](//owlcarousel2.github.io/OwlCarousel2/)** sur **.slider** de la page **index* (navigation en dots) : Pour cela aider vous de la documentation !
-  Et **[Remodal](https://github.com/vodkabears/Remodal)**. Celui-ci devra presenter un formulaire de contact (non fonctionnel) via l'entré **"contact"** du menu.
-  Enfin, utiliser le plugin **[Autosize](http://www.jacklmoore.com/autosize/)** et l'appliquer au **textarea** de cette modale.

---------

### **TP 2** :
 
À l'aide de Jquery, vous devez ajouter un écouteur d'évènement click() sur les liens de la navigation. Cet évènement doit exécuter une fonction anonyme qui se chargera de récupérer la valeur de l'attribut "data-page" du lien cliqué. Cette dernière correspondant au nom de la page que nous allons injecter de manière asynchrone ainsi que d'ajouter la class .active au lien cliqué.

#### Partie 1 :

- Créer un écouteur d'évènement click() sur les liens de la navigation (uniquement).
- L'évènement doit annuler le comportement par défaut de ces liens (cf : td slide 87)
- Utiliser $(this) pour récupérer la valeur de l'attribut 'data-page' du lien.
- Utiliser console.log() ou alert() pour vérifier le fonctionnement de votre code.

#### Partie 2 :

- Créer une fonction loadPage() en dehors du scope précèdent. Cette fonction doit accueillir un paramètre obligatoire que nous nommerons "target".
- Faites appel à cette fonction dans l'écouteur d'évènement.
- LoadPage() doit ajouter la classe .active sur le lien cliqué. **Attention** : Seul un lien avec la classe .active doit être présent dans la navigation !
- Le paramètre target permet de communiquer la valeur de "data-page" à la fonction loadPage(). En sachant cela, utilisez console.log ou alert() au sein de loadPage() pour vérifier la bonne communication entre l'écouteur d'évènement et la fonction loadPage().

#### Partie 3 :

- À l'aide de la documentation jQuery, utiliser la méthode jQuery get() pour charger les pages présentent dans le sous-dossier "pages" et injecter leur contenue sur l'id #pages.
- Faites en sorte que le slider owlCarousel soit fonctionnel lorsque la page d'accueil est injectée.

Bon courage !
